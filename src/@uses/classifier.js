import ml5 from "ml5";
import { ref } from "vue";
import { getGiphy } from "../utils/utils";

let classifier = {};
const isModelLoaded = ref(false);
const results = ref([]);
const loadingIcon = ref(true);

const init = function () {
  classifier = ml5.imageClassifier("MobileNet", modelLoaded);
}
function modelLoaded() {
  console.log("Model Loaded!");
  isModelLoaded.value = true;
}
async function classify(image) {
  results.value.length = 0;
  // loadingIcon.value.giphy = require('../assets/timer.svg');
  loadingIcon.value = true;
  console.log('loadingIcon 1 ', loadingIcon);
  const responses = await classifier.classify(image);
  responses.forEach(async (_rs) => {
    const objectData = {
      label: _rs.label,
      confidence: (_rs.confidence * 100).toFixed(2),
      giphy: "",
      giphyUrl: "",
      alt: ""
    }
    const rsGiphy = await getGiphy(_rs.label);
    const { data } = rsGiphy['data'];
    data.forEach(_o => {
      results.value.push({...objectData, 
        giphy: _o.images.original.url, alt: _o.title, giphyUrl: _o.url});
    });
    loadingIcon.value = false;
  });
}

export default () => ({
  isModelLoaded,
  results,
  init,
  classify,
  loadingIcon
});

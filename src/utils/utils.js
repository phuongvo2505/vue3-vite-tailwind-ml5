import axios from "axios";

const getGiphy = async (label) => {
  try {
    const url =
      "https://api.giphy.com/v1/gifs/search?api_key=gnHx8iIxLiE3MLUDnVWJ6pcWDlI8LGqL&limit=1&q=";
    const response = await axios.get(url + label + "&offset=" + Math.floor(Math.random() * 100 + 1));
    return response;
  } catch (error) {
    throw error;
  }
};

export { getGiphy };
